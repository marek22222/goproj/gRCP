package main

import (
	"context"
	"log"
	"math/rand"
	"net"

	pb "gitlab.com/marek22222/goProj/grcp/03_moss_my/05_1_uzytkownicy/uzytkownicy"
	"google.golang.org/grpc"
)

const (
	port = ":50051"
)

func NowyZarzadzanieServer() *ZarzadzanieServer {
	return &ZarzadzanieServer{
		user_lista: &pb.ListaUserow{},
	}
}

type ZarzadzanieServer struct {
	pb.UnimplementedZarzadzanieServer
	user_lista *pb.ListaUserow
}

func (server *ZarzadzanieServer) Run() error {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failed to listen: %v", err)
	}

	s := grpc.NewServer()
	pb.RegisterZarzadzanieServer(s, server)
	log.Printf("serwer nasłuchuje na: %v", lis.Addr())
	return s.Serve(lis)
}

func (s *ZarzadzanieServer) UtworzNowegoUsera(
	kxt context.Context, in *pb.NowyUser) (*pb.User, error) {

	log.Printf("Otrzymano: %v", in.GetNazwa())
	var user_id int32 = int32(rand.Intn(1000))
	nowy_user := &pb.User{Nazwa: in.GetNazwa(), Wiek: in.GetWiek(), Id: user_id}
	s.user_lista.Userzy = append(s.user_lista.Userzy, nowy_user)
	return nowy_user, nil
}

func (s *ZarzadzanieServer) PobierzUserow(ctx context.Context, in *pb.PobierzUserowParams) (*pb.ListaUserow, error) {
	return s.user_lista, nil
}

func main() {
	var serwer *ZarzadzanieServer = NowyZarzadzanieServer()
	err := serwer.Run()
	if err != nil {
		log.Fatalf("failed to server: %v", err)
	}
}
