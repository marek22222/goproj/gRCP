package main

import (
	"context"
	"log"
	"time"

	pb "gitlab.com/marek22222/goProj/grcp/03_moss_my/04_uzytkownicy/uzytkownicy"
	"google.golang.org/grpc"
	"google.golang.org/grpc/credentials/insecure"
)

const (
	adres = "localhost:50051"
)

func main() {
	conn, err := grpc.Dial(adres, grpc.WithTransportCredentials(insecure.NewCredentials()))
	if err != nil {
		log.Fatalf("brak połączenia: %v", err)
	}
	defer conn.Close()
	c := pb.NewZarzadzanieClient(conn)

	kxt, zakoncz := context.WithTimeout(context.Background(), time.Second)
	defer zakoncz()

	var nowi_userzy = make(map[string]int32)
	nowi_userzy["Alicja"] = 43
	nowi_userzy["Bob"] = 30

	for nazwa, wiek := range nowi_userzy {
		r, err := c.UtworzNowegoUsera(kxt,
			&pb.NowyUser{Nazwa: nazwa, Wiek: wiek})
		if err != nil {
			log.Fatalf("nie można utworzyć usera: %v", err)
		}
		log.Printf(`Szczegóły usera: NAZWA: %s  WIEK: %d  ID: %d`,
			r.GetNazwa(), r.GetWiek(), r.GetId())
	}
}
