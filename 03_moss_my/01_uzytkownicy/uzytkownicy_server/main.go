package main

import (
	"context"
	"io/ioutil"
	"log"
	"math/rand"
	"net"
	"os"

	pb "gitlab.com/marek22222/goProj/gRCP/03_moss_my/01_uzytkownicy/uzytkownicy"
	"google.golang.org/grpc"
	"google.golang.org/protobuf/encoding/protojson"
)

const (
	port = ":50051"
)

func NowySerwerUzytkownikow() *SerwerUzytkownikow {
	return &SerwerUzytkownikow{
		//listaUzytk: &pb.ListaUzytkownikow{},

	}
}

type SerwerUzytkownikow struct {
	pb.UnimplementedSerwerUzytkownikow
	//listaUzytk *pb.ListaUzytkownikow
}

func (server *SerwerUzytkownikow) Uruchom() error {
	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("failen to listen: %v", err)
	}

	s := grpc.NewServer()
	pb.RegisterSerwerUzytkownikow(s, server)
	log.Printf("serwer nasłuchuje na %v", lis.Addr())
	return s.Serve(lis)
}

func (s *SerwerUzytkownikow) UtworzUzytkownika(ctx context.Context,
	in *pb.NowyUzytkownik) (*pb.Uzytkownik, error) {

	log.Printf("Otrzymano: %v", in.GetNazwa())
	czytaneBajty, err := ioutil.ReadFile("user.json")
	var listaUzytk *pb.ListaUzytkownikow = &pb.ListaUzytkownikow{}
	var uzytkownik_id int32 = int32(rand.Intn(1000))
	nowy_user := &pb.Uzytkownik{Nazwa: in.GetNazwa(), Wiek: in.GetWiek(), Id: uzytkownik_id}

	if err != nil {
		if os.IsNotExist(err) {
			log.Print("Nie znaleziono pliku. Utwórz nowy plik")
			listaUzytk.Uzytkownicy = append(listaUzytk.Uzytkownicy, nowy_user)
			jsonBajty, err := protojson.Marshal(listaUzytk)
			if err != nil {
				log.Fatalf("Bład marshallowania JSON: %v", err)
			}
			err = ioutil.WriteFile("user.json", jsonBajty, 0664)
			if err != nil {
				log.Fatalf("Błąd zapisu do pliku: %v", err)
			}
			return nowy_user, nil
		} else {
			log.Fatalf("Błąd odczytu pliku: %v", err)
		}
	}

	err = protojson.Unmarshal(czytaneBajty, listaUzytk)
	if err != nil {
		log.Fatalf("Błąd parsowanie listy uzytkownikow: %v", err)
	}
	listaUzytk.Uzytkownicy = append(listaUzytk.Uzytkownicy, nowy_user)
	jsonBajty, err := protojson.Marshal(listaUzytk)
	if err != nil {
		log.Fatalf("Bład marshallowania JSON: %v", err)
	}
	err = ioutil.WriteFile("user.json", jsonBajty, 0664)
	if err != nil {
		log.Fatalf("Błąd zapisu do pliku: %v", err)
	}
	return nowy_user, nil
}

func (s *SerwerUzytkownikow) PobierzUzytkownikow(
	ctx context.Context, in *pb.PobierzPrmUzytkownikow) (*pb.ListaUzytkownikow, error) {
	jsonBajty, err := ioutil.ReadFile("user.json")
	if err != nil {
		log.Fatalf("Błąd czytania z pliku: %v", err)
	}
	var listaUzytk *pb.ListaUzytkownikow = &pb.ListaUzytkownikow{}
	err = protojson.Unmarshal(jsonBajty, listaUzytk)
	if err != nil {
		log.Fatalf("Błąd Unmarshal: %v", err)
	}
	return listaUzytk, nil
}

func main() {
	var serwer *SerwerUzytkownikow = NowySerwerUzytkownikow()
	err := serwer.Uruchom()
	if err != nil {
		log.Fatalf("błąd serwowania: %v", err)
	}
}
