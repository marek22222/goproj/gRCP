package main

import (
	"log"
	"net"

	"google.golang.org/grpc"
)

// type server struct {
// 	pb.UnimplementedGreeterServer
// }


func main() {
	port := ":9000"

	lis, err := net.Listen("tcp", port)
	if err != nil {
		log.Fatalf("Failed to listen on port: %s", port)
	}

	grpcServer := grpc.NewServer()

	if err := grpcServer.Serve(lis); err != nil {
		log.Fatalf("Failed to serve gRPC server over port %s: %v", port, err)
	}
}
